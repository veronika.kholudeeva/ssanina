from django.contrib import admin

from .models import Customer, Order, Product, Cart, Order_line

admin.site.register(Customer)
admin.site.register(Order)
admin.site.register(Product)
admin.site.register(Cart)
admin.site.register(Order_line)
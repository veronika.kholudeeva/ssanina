import json
import os
import re

from django.shortcuts import render
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from .models import Customer, Order, Product, Cart, Order_line
from django.http import *

from django.db.models import Max
#os.environ.setdefault("DJANGO_SETTINGS_MODULE", "cakes.settings")
import sys
import datetime
#from django.conf import settings
from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator
from django.forms.models import model_to_dict
from django.views.generic import View
from django.views.generic import CreateView
from django import forms
from django.contrib.auth import get_user
from cakes.models import Customer, Order, Product, Cart, Order_line
from django.template.response import TemplateResponse
import random
import string


#Шаблон
def index(request):
    if request.method == 'GET':
        return HttpResponseRedirect('/about/')
    else:
        return HttpResponseBadRequest

# Главная/О нас
def about(request):
    if request.method == 'GET':
        if request.user.is_authenticated:
            userr =  request.user
        else:
            userr = None
        return render(request, "about.html", {"userr":userr})
    else:
        return HttpResponseBadRequest

#Заказ
def zakaz(request):
    if request.method == 'POST':
        s = request.POST.get('s')
        p = request.POST.get('p')
        Order(id_customer=Customer.objects.get(login=request.user), amount= Cart.objects.filter(login=Customer.objects.get(login=request.user)).count(), amount_of_money=s, order_date=datetime.date.today(), date_of_payment=datetime.date.today(), note = p).save()
        idd = Order.objects.filter(id_customer=Customer.objects.get(login=request.user)).order_by('id_order').last()
        print(idd)
        for i in Cart.objects.filter(login=Customer.objects.get(login=request.user)):
            Order_line(id_product = i.id_product, id_order= idd, quantity_line = i.quantity, price_summary_line=i.price_summary).save()
            Cart.objects.get(id_cart=i.id_cart).delete()
        return HttpResponse('success')
    else:
        return HttpResponseBadRequest

# Корзина
def korzina(request):
    if request.method == 'POST':
        s = request.POST.get('s')
        product=Product.objects.get(id_product=s)
        if request.user.is_authenticated:
            if Cart.objects.filter(id_product = Product.objects.get(id_product=s), login = Customer.objects.get(login = request.user)).exists():
                Cart.objects.filter(id_product = Product.objects.get(id_product=s),login = Customer.objects.get(login = request.user) ).update(quantity= list(Cart.objects.values('quantity').get(id_product=s).values())[0]+1, price_summary=list(Product.objects.values('price').get(id_product = s).values())[0] * (list(Cart.objects.values('quantity').get(id_product=s).values())[0]+1))
            else:
                cart = Cart(login = Customer.objects.get(login = request.user), id_product= Product.objects.get(id_product=s), name_product= Product.objects.get(name_product = product.name_product), photo=Product.objects.get( photo = product.photo), price_summary=list(Product.objects.values('price').get(id_product = s).values())[0])
                cart.save(force_insert=True)
            kol = Cart.objects.filter(login = Customer.objects.get(login = request.user)).count()
            data = json.dumps({
                'list': list(Cart.objects.filter(login = Customer.objects.get(login = request.user)).values()),
                'kol':kol,
                })   
            return JsonResponse(data, safe=False)
        else:
            if not request.session.get('login', 'nope')=="nope":
                if Cart.objects.filter(id_product = Product.objects.get(id_product=s), login_anonymus = request.session['login']).exists():
                    Cart.objects.filter(id_product = Product.objects.get(id_product=s),login_anonymus = request.session['login'] ).update(quantity= list(Cart.objects.values('quantity').get(id_product=s).values())[0]+1, price_summary=list(Product.objects.values('price').get(id_product = s).values())[0] * (list(Cart.objects.values('quantity').get(id_product=s).values())[0]+1))    
                else:
                    cart2 = Cart(login_anonymus = request.session['login'], id_product= Product.objects.get(id_product=s), name_product= Product.objects.get(name_product = product.name_product), photo=Product.objects.get( photo = product.photo), price_summary=list(Product.objects.values('price').get(id_product = s).values())[0])
                    cart2.save(force_insert=True)
            else:
                for i in range(16):
                    request.session['login'] = "".join(random.choice(string.ascii_uppercase + string.ascii_lowercase + string.digits) for x in range(16))
                cart = Cart(login_anonymus = request.session['login'], id_product= Product.objects.get(id_product=s), name_product= Product.objects.get(name_product = product.name_product), photo=Product.objects.get( photo = product.photo), price_summary=list(Product.objects.values('price').get(id_product = s).values())[0])
                cart.save(force_insert=True)
            kol = Cart.objects.filter(login_anonymus = request.session['login']).count()
            data = json.dumps({
                'list': list(Cart.objects.filter(login_anonymus = request.session['login']).values()),
                'kol':kol,
                })
            return JsonResponse(data, safe=False)
    else:
        return HttpResponseBadRequest

def cart(request):
    if request.method == 'POST':
        if request.user.is_authenticated:
            kol = Cart.objects.filter(login = Customer.objects.get(login = request.user)).count()
            data = json.dumps({
                'list': list(Cart.objects.filter(login = Customer.objects.get(login = request.user)).values('photo__photo', 'name_product__name_product', 'id_product__id_product', 'quantity', 'price_summary')),
                'kol':kol,
                })  
            return JsonResponse(data, safe=False)
        else:
            kol = Cart.objects.filter(login_anonymus = request.session['login']).count()
            data = json.dumps({
                'list': list(Cart.objects.filter(login_anonymus = request.session['login']).values('photo__photo', 'name_product__name_product', 'id_product__id_product', 'quantity', 'price_summary')),
                'kol':kol, 
                })     
            
            return JsonResponse(data, safe=False)
    else:
        return HttpResponseBadRequest
        
def kor(request):
    if request.method == 'POST':
        s = request.POST.get('s')
        p = request.POST.get('p')
        print(s)
        print(p)
        if s=="minus":
            if request.user.is_authenticated:
                Cart.objects.filter(id_product = Product.objects.get(id_product=p),login = Customer.objects.get(login = request.user)).update(quantity= list(Cart.objects.values('quantity').get(id_product=p).values())[0]-1, price_summary=list(Product.objects.values('price').get(id_product = p).values())[0] * (list(Cart.objects.values('quantity').get(id_product=p).values())[0]-1))
            else:
                Cart.objects.filter(id_product = Product.objects.get(id_product=p),login_anonymus = request.session['login'] ).update(quantity= list(Cart.objects.values('quantity').get(id_product=p).values())[0]-1, price_summary=list(Product.objects.values('price').get(id_product = s).values())[0] * (list(Cart.objects.values('quantity').get(id_product=p).values())[0]-1))
        if s=="plus":
            if request.user.is_authenticated:
                Cart.objects.filter(id_product = Product.objects.get(id_product=p),login = Customer.objects.get(login = request.user)).update(quantity= list(Cart.objects.values('quantity').get(id_product=p).values())[0]+1, price_summary=list(Product.objects.values('price').get(id_product = p).values())[0] * (list(Cart.objects.values('quantity').get(id_product=p).values())[0]+1))
            else:
                Cart.objects.filter(id_product = Product.objects.get(id_product=p),login_anonymus = request.session['login'] ).update(quantity= list(Cart.objects.values('quantity').get(id_product=p).values())[0]+1, price_summary=list(Product.objects.values('price').get(id_product = p).values())[0] * (list(Cart.objects.values('quantity').get(id_product=p).values())[0]+1))
        if s=="del":
            if request.user.is_authenticated:
                Cart.objects.filter(id_product = Product.objects.get(id_product=p),login = Customer.objects.get(login = request.user)).delete()
            else:
                Cart.objects.filter(id_product = Product.objects.get(id_product=p),login_anonymus = request.session['login']).delete()
        return HttpResponse('success')
    else:
        return HttpResponseBadRequest

# Каталог
def catalog(request):
    if request.method == 'GET':
        if request.user.is_authenticated:
            userr =  request.user
        else:
            userr = None
        product = Product.objects.all()
        return render(request, "catalog.html", {"product":product, "userr":userr})
    else:
        return HttpResponseBadRequest

# Личный кабинет
def cabinet(request):
    if request.method == 'GET':
        if request.user.is_authenticated:
            person = Customer.objects.filter(login=request.user)
            userr = request.user
            zakaz = Order.objects.filter(id_customer=Customer.objects.get(login=request.user))    
        else:
            userr = None
            person = None
            zakaz = None
        return render(request, "lk.html", {"person": person, "userr": userr, "zakaz": zakaz})
    else:
        return HttpResponseBadRequest

#Редактирование пользователя
def red(request):
    if request.method == 'POST':
        change_user = json.loads(request.POST.get('change_user'))
        print(change_user.get('id'))
        Customer.objects.filter(id_customer = change_user.get('id')).update(name=change_user.get('name'), surname=change_user.get('surname'), \
        middle_name=change_user.get('middle_name'), sex=change_user.get('sex'), \
        date_of_birth=change_user.get('date'), email=change_user.get('email'), \
        phone=change_user.get('phone'), login=change_user.get('login'), password=change_user.get('password'))
        return HttpResponse('Success')
    else:
        return HttpResponseBadRequest

#Войти
def log(request):
    if request.method == 'POST':
        login_user = json.loads(request.POST.get('login_user'))
        user = authenticate(username=login_user.get('login'), password=login_user.get('password'))
        if user is not None:
            if user.is_active:
                login(request, user)
                if not request.session.get('login', 'nope')=="nope":
                    del request.session['login']
                return HttpResponse("All Right")
            else:
                return HttpResponse('User is disabled')
        else:
            return HttpResponse("The username or password were incorrect.")
    else:
        return HttpResponseBadRequest

#Выход пользователя
def logout_v(request):
    if request.method == 'POST':
        logout(request)
        return HttpResponse('success logout')
    else:
        return HttpResponseBadRequest

# Регистрация пользователей
def reg(request):
    if request.method == 'POST':
        autorization = json.loads(request.POST.get('autorization'))
        rccd = Customer(name=autorization.get('name'), surname=autorization.get('surname'), \
        middle_name=autorization.get('middle_name'), sex=autorization.get('sex'), \
        date_of_birth=autorization.get('date'), email=autorization.get('email'), \
        phone=autorization.get('phone'), login=autorization.get('login'), password=autorization.get('password'))
        user = User.objects.create_user(autorization.get('login'), autorization.get('email'), autorization.get('password'))
        user.save()
        rccd.save(force_insert=True)
        return HttpResponse('Success')
    else:
        return HttpResponseBadRequest
//Регистрация
$("#regkn").click(function(){
  sendAjaxForm(window.location.origin + '/reg/');
  return false;
});

function sendAjaxForm(url) {
  let autorization = {
      name:'',
      surname:'',
      middle_name:'',
      sex:'',
      date:'',
      email:'',
      phone:'',
      login:'',
      password:'',
  }
  
  autorization.name = $("input[name='name']").val();
  autorization.surname = $("input[name='surname']").val();
  autorization.middle_name = $("input[name='middle_name']").val();
  autorization.date = $("input[name='date']").val();
  autorization.email = $("input[name='e-mail']").val();
  autorization.phone = $("input[name='phone']").val();
  autorization.login = $("input[name='login']").val();
  autorization.password = $("input[name='pass']").val();
  $("input[name='sex']").each(function (){
    if ($(this).prop("checked")) {
      autorization.sex = $(this).val();
    }
  });

  $.ajax({
    url: url, //url страницы
    type: "POST", //метод отправки
    dataType: "text", //формат данных
    data: {
      csrfmiddlewaretoken: getCookie('csrftoken'),
      'autorization': JSON.stringify(autorization),
    },
    cache: false,
    success: function(data) {
      console.log(data)
    },
    error: function() {
      console.log('Error')
    }
  });
}

//Вход
$("#log").click(function() {
  sendAjaxFormLog(window.location.origin + '/log/');
  return false;
});

function sendAjaxFormLog(url) {
  let login_user = {
      login:'',
      password:'',
  }
  login_user.login = $("input[name='log2']").val();
  login_user.password = $("input[name='pass2']").val();
  $.ajax({
  url: url, //url страницы
  type: "POST", //метод отправки
  dataType: "text", //формат данных
  data: {
  csrfmiddlewaretoken: getCookie('csrftoken'),
  'login_user': JSON.stringify(login_user),
  },
  cache: false,
  success: function(data) {
    console.log(data);
    document.location.reload();
  },
  error: function() {
    console.log('Проверьте введенные данные');
  }
  });
}

//Выход
$(".logout").click(function() {
  sendAjaxFormLogout(window.location.origin + '/logout/');
  return false;
});

function sendAjaxFormLogout(url) {
  $.ajax({
    url: url, //url страницы
    type: "POST", //метод отправки
    dataType: "text", //формат данных
    data: {
      csrfmiddlewaretoken: getCookie('csrftoken'),
    },
    cache: false,
    success: function(data) {
      //console.log('Sucsess');
      document.location.reload();
    },
    error: function() {
      console.log('Проверьте введенные данные');
    }
  });
}


//Редактирование профиля
$("#redact").click(function(){
  if ($('.form2').css('visibility')=='hidden') {
    $('.form2').css('visibility','visible');
    $('.wrap').css('visibility','visible');
  }
  else {
    $('.form2').css('visibility','hidden');
    $('.wrap').css('visibility','hidden');
  }
  //console.log(window.location.origin + '/reg/');
  sendAjaxFormChange(window.location.origin + '/red/');
  return false;
});

function sendAjaxFormChange(url) {
  let change_user = {
      name:'',
      surname:'',
      middle_name:'',
      sex:'',
      date:'',
      email:'',
      phone:'',
      login:'',
      password:'',
      id:'',
  }
  change_user.name = $("input[name='name2']").val();
  change_user.surname = $("input[name='surname2']").val();
  change_user.middle_name = $("input[name='middle_name2']").val();
  change_user.date = $("input[name='date2']").val();
  change_user.email = $("input[name='e-mail2']").val();
  change_user.phone = $("input[name='phone2']").val();
  change_user.login = $("input[name='login3']").val();
  change_user.password = $("input[name='pass3']").val();
  change_user.id = $("input[name='login3']").attr('id');
  //console.log($("input[name='login3']").attr('id'));
  $("input[name='sex2']").each(function (){
    if ($(this).prop("checked")) {
      change_user.sex = $(this).val();
    }
  });

  $.ajax({
    url: url, //url страницы
    type: "POST", //метод отправки
    dataType: "text", //формат данных
    data: {
      csrfmiddlewaretoken: getCookie('csrftoken'),
      'change_user': JSON.stringify(change_user),
    },
    cache: false,
    success: function(data) {
      document.location.reload();
    },
    error: function() {
      console.log('Error')
    }
  });
}


//Обработка клика на корзину
$(document).on('click', '.korzina', function() {
  //скытие, раскрытие блоков
  if ($('.korzina_w').css('visibility')=='hidden') {
    $('.korzina_w').css('visibility','visible');
    $('.wrap').css('visibility','visible');
  }
  else {
    $('.korzina_w').css('visibility','hidden');
    $('.wrap').css('visibility','hidden');
  }
  //ajax-запрос
  sendAjaxFormCart(window.location.origin + '/cart/');
});

function sendAjaxFormCart(url) {
  $.ajax({
    url: url, //url страницы
    type: "POST", //метод отправки
    dataType: "json", //формат данных
    data: {
      csrfmiddlewaretoken: getCookie('csrftoken'),
    },
    cache: false,
    success: function(data) {
      list = JSON.parse(data);
      let text="";
      console.log(data);
      for (i=0;i<list.kol;i++){
        text += "<tr><td><img src='/static/images/catalog/" + list.list[i].photo__photo + "' width='50px'> \
        </td><td>" + list.list[i].name_product__name_product + "</td><td class='price' value='" + list.list[i].price_summary + "' \
        >" + list.list[i].price_summary + " руб.</td><td class='knopa' value='minus' name='" +list.list[i].id_product__id_product + "' \
        ><img src='/static/images/minus.png' width='20px'></td><td><div class = 'quantity'>" + list.list[i].quantity + " шт. \
        </div></td><td class='knopa' value='plus' name='" + list.list[i].id_product__id_product+ "'><img \
        src='/static/images/plus.png' width='20px'></td><td class='knopa' value='del' \
        name='" + list.list[i].id_product__id_product + "'><img src = '/static/images/exit.png' width='35px'></td></tr>"
        }
      $('.car').replaceWith(text);
      cart();
      //document.location=window.location.origin+'/korzina';
    },
    error: function() {
      console.log('Проверьте введенные данные')
    }
  });
}




//Добавление в корзину
$(document).on('click', '.btn', function() {
  sendAjaxFormK(window.location.origin + '/korzina/', $(this).val());
});

function sendAjaxFormK(url,s) {
  $.ajax({
    url: url, //url страницы
    type: "POST", //метод отправки
    dataType: "text", //формат данных
    data: {
      csrfmiddlewaretoken: getCookie('csrftoken'),
      "s":s,
    },
    cache: false,
    success: function(data) {
      //document.location=window.location.origin+'/korzina';
    },
    error: function() {
      console.log('Проверьте введенные данные')
    }
  });
}

$(document).on('click', '.knopa', function() {
  sendAjaxFormK(window.location.origin + '/kor/', $(this).attr('value'), $(this).attr('name'));
});

function sendAjaxFormK(url,s, p) {
  $.ajax({
    url: url, //url страницы
    type: "POST", //метод отправки
    dataType: "text", //формат данных
    data: {
      csrfmiddlewaretoken: getCookie('csrftoken'),
      "s":s,
      "p":p,
    },
    cache: false,
    success: function(data) {
      //document.location=window.location.origin+'/korzina';
    },
    error: function() {
      console.log('Проверьте введенные данные')
    }
  });
}


//заказ
$(document).on('click', '.zakaz', function() {
  sendAjaxFormZakaz(window.location.origin + '/zakaz/', $('#mydiv').attr('class'), $('.komm').attr('value'));
});

function sendAjaxFormZakaz(url, s, p) {
  $.ajax({
    url: url, //url страницы
    type: "POST", //метод отправки
    dataType: "text", //формат данных
    data: {
      csrfmiddlewaretoken: getCookie('csrftoken'),
      's':s,
      'p':p,
    },
    cache: false,
    success: function(data) {
      //document.location=window.location.origin+'/korzina';
    },
    error: function() {
      console.log('Проверьте введенные данные')
    }
  });
}

//обработка POST-запросов для Django
function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
    var cookies = document.cookie.split(';');
    for (var i = 0; i < cookies.length; i++) {
    var cookie = jQuery.trim(cookies[i]);
    // Does this cookie string begin with the name we want?
    if (cookie.substring(0, name.length + 1) === (name + '=')) {
    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
    break;
    }
    }
    }
    return cookieValue;
}
